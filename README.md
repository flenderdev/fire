# Fire.com API Client for Ruby

## Documentation
https://fire.com/docs

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'fire-business'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fire-business
