require 'fire/version'
require 'fire/webhook_event'
require 'fire/error'
require 'fire/connection'
require 'fire/base_client'
require 'fire/base_service'
require 'fire/credentials'
require 'fire/accounts/create_account_service'
require 'fire/accounts/list_accounts_service'
require 'fire/accounts/retrieve_account_service'
require 'fire/accounts/retrieve_transactions_service'
require 'fire/accounts/client'
require 'fire/authentication/retrieve_access_token_service'
require 'fire/authentication/client'
require 'fire/payment_batches/constants'
require 'fire/payment_batches/batch_service'
require 'fire/payment_batches/add_account_bank_payment_service'
require 'fire/payment_batches/add_internal_payment_service'
require 'fire/payment_batches/add_payee_bank_payment_service'
require 'fire/payment_batches/cancel_batch_service'
require 'fire/payment_batches/list_batch_approvers_service'
require 'fire/payment_batches/list_batch_bank_transfers_service'
require 'fire/payment_batches/list_batch_internal_transfers_service'
require 'fire/payment_batches/list_batches_service'
require 'fire/payment_batches/remove_batch_payment_service'
require 'fire/payment_batches/retrieve_batch_service'
require 'fire/payment_batches/submit_batch_service'
require 'fire/payment_batches/create_batch_service'
require 'fire/payment_batches/client'

module Fire
  CURRENCIES = [
    EUR = 'EUR'.freeze,
    GBP = 'GBP'.freeze
  ]

  # All amounts are in cents
  DEFAULT_FEE_AMOUNT = 49

  BANK_TRANSFER_FEE_PERCENT = 0.05 # 0.05%
  BANK_TRANSFER_FEE_MAX_AMOUNT = 2500
  BANK_TRANSFER_FEE_MIN_AMOUNT = 49

  NEW_ACCOUNT_FEE_AMOUNT = 5000

  LODGEMENT_FEE_PERCENT = 1 # 1%
  LODGEMENT_FEE_MAX_AMOUNT = 49
  LODGEMENT_FEE_MIN_AMOUNT = 10
end