require 'rest-client'
require 'ostruct'

module Fire
  module Connection
    ENDPOINT_URL = 'https://api.fire.com/business/v1/'

    def get(path, payload = {})
      build_respose(RestClient.get(url + path + build_query(payload), headers))
    end

    def post(path, payload = {})
      build_respose(RestClient.post(url + path, transform_payload(payload), headers))
    end

    def put(path, payload = {})
      build_respose(RestClient.put(url + path, transform_payload(payload), headers))
    end

    def delete(path)
      build_respose(RestClient.delete(url + path, headers))
    end

    protected

    def headers
      { content_type: :json }
    end

    private

    # @return [String]
    #   This function will return Endpoint URL set in environment variable otherwise it returns default URL.
    #   To set a development(pre-production) URL endpoint use ENV variable.
    def url
      ENV['FIRE_ENDPOINT_URL'] || ENDPOINT_URL
    end

    def build_query(payload)
      return '' if payload.nil? || payload.empty?

      "?#{transform_keys(payload).map{|key, value| "#{key}=#{value}"}.join('&')}"
    end

    def transform_payload(payload)
      transform_keys(payload).to_json
    end

    def transform_keys(payload)
      payload.compact.deep_stringify_keys.deep_transform_keys { |key| key.camelize(:lower) }
    end

    def build_respose(data)
      return nil if data.nil? || data.empty?

      JSON.parse(data, object_class: OpenStruct)
    end
  end
end