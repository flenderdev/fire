module Fire
  class BaseService
    attr_reader :client, :params

    def initialize(client, params = {})
      @client = client
      @params = params
    end

    def self.call(*args)
      new(*args).call
    end

    def get(*args)
      client.get(*args)
    end

    def post(*args)
      client.post(*args)
    end

    def put(*args)
      client.put(*args)
    end

    def delete(*args)
      client.delete(*args)
    end
  end
end