module Fire
  module Accounts
    class ListAccountsService < BaseService
      def call
        get('accounts')
      end
    end
  end
end