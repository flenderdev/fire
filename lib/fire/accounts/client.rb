module Fire
  module Accounts
    class Client < BaseClient
      def list_accounts
        ListAccountsService.call(self)
      end

      def retrieve_account(account_id)
        RetrieveAccountService.call(self, account_id)
      end

      def create_account(params = {})
        CreateAccountService.call(self, params)
      end

      def transactions(account_id)
        RetrieveTransactionsService.call(self, account_id)
      end

      protected

      def headers
        {
          content_type: :json,
          authorization: "Bearer #{credentials.access_token}"
        }
      end
    end
  end
end