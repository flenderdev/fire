module Fire
  module Accounts
    class RetrieveAccountService < BaseService
      attr_reader :account_id

      def initialize(client, account_id)
        super(client)
        @account_id = account_id
      end

      def call
        get("accounts/#{account_id}")
      end
    end
  end
end