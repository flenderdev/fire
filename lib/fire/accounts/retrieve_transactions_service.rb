module Fire
  module Accounts
    class RetrieveTransactionsService < BaseService
      attr_reader :account_id

      def initialize(client, account_id)
        super(client)
        @account_id = account_id
      end

      def call
        offset = 0
        limit = 200
        result = retrieve_transactions(offset, limit)
        offset += limit
        while offset < result&.total&.to_i
          current = retrieve_transactions(offset, limit)
          break if current.transactions.empty?

          result.transactions += current.transactions
          offset += limit
        end
        result
      end

      private

      def retrieve_transactions(offset = 0, limit = 200)
        get("accounts/#{account_id}/transactions", {offset: offset, limit: limit})
      end
    end
  end
end