module Fire
  module Accounts
    class CreateAccountService < BaseService
      def call
        validate!
        post('accounts', payload)
      end

      private

      def validate!
        raise Error.new('Invalid account name provided') if params[:account_name].nil? || params[:account_name].empty?
      end

      def payload
        {currency: EUR, accept_fees_and_charges: true}.merge(params)
      end
    end
  end
end