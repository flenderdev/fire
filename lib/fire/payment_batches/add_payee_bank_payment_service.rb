module Fire
  module PaymentBatches
    class AddPayeeBankPaymentService < BatchService
      include Constants

      # params
      # {
      #   "icanFrom": "2001",
      #   "payeeId": "15002",
      #   "payeeType": "PAYEE_ID",
      #   "amount": "500",
      #   "myRef": "Payment to John Smith for Consultancy in Dec.",
      #   "yourRef": "ACME LTD - INV 23434"
      # }
      def call
        validate!
        post("#{batch_url}/banktransfers", params)
      end

      private

      def validate!
        raise Exception.new('Invalid parameters provided') if params[:ican_from].nil? || params[:ican_from].empty?
        raise Exception.new('Invalid payee ID provided') if params[:payee_id].nil? || params[:payee_id].empty?
        raise Exception.new('Invalid payee type provided') unless params[:payee_type] == PAYEE_TYPE_PAYEE_ID
        raise Exception.new('Invalid transaction amount provided') if params[:amount].to_f <= 0
      end
    end
  end
end