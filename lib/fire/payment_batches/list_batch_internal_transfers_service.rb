module Fire
  module PaymentBatches
    class ListBatchInternalTransfersService < BatchService
      def call
        get("#{batch_url}/internaltransfers")
      end
    end
  end
end