module Fire
  module PaymentBatches
    class ListBatchApproversService < BatchService
      def call
        get("#{batch_url}/approvals")
      end
    end
  end
end