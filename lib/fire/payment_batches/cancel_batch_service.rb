module Fire
  module PaymentBatches
    class CancelBatchService < BatchService
      def call
        delete(batch_url)
      end
    end
  end
end