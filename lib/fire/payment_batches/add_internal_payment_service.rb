module Fire
  module PaymentBatches
    class AddInternalPaymentService < BatchService
      # params
      # {
      #   icanFrom: "2001",
      #   icanTo: "2041",
      #   amount: 10_000,
      #   ref: "Moving funds to Operating Account"
      # }
      def call
        validate!
        post("#{batch_url}/internaltransfers", params)
      end

      private

      def validate!
        raise Exception.new('Invalid parameters provided') if params[:ican_from].nil? || params[:ican_from].empty?
        raise Exception.new('Invalid parameters provided') if params[:ican_to].nil? || params[:ican_to].empty?
        raise Exception.new('Invalid transaction amount provided') if params[:amount].to_f <= 0
      end
    end
  end
end