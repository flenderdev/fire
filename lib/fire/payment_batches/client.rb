module Fire
  module PaymentBatches
    class Client < BaseClient
      include Constants

      def create_batch(params = {})
        CreateBatchService.call(self, params)
      end

      def submit_batch(batch_id)
        SubmitBatchService.call(self, batch_id)
      end

      def retrieve_batch(batch_id)
        RetrieveBatchService.call(self, batch_id)
      end

      def list_batches(params = {})
        ListBatchesService.call(self, params)
      end

      def list_batch_approvers(batch_id)
        ListBatchApproversService.call(self, batch_id)
      end

      def remove_batch_bank_transfer(batch_id, transfer_id)
        RemoveBatchPaymentService.call(self, batch_id, transfer_id)
      end

      def remove_batch_internal_transfer(batch_id, transfer_id)
        RemoveBatchPaymentService.call(self, batch_id, transfer_id, TYPE_INTERNAL_TRANSFER)
      end

      def list_batch_internal_transfers(batch_id)
        ListBatchInternalTransfersService.call(self, batch_id)
      end

      def list_batch_bank_transfers(batch_id)
        ListBatchBankTransfersService.call(self, batch_id)
      end

      def cancel_batch(batch_id)
        CancelBatchService.call(self, batch_id)
      end

      def add_batch_internal_transfer(batch_id, params = {})
        AddInternalPaymentService.call(self, batch_id, params)
      end

      def add_batch_bank_transfer(batch_id, params = {})
        case params[:payee_type]
        when PAYEE_TYPE_PAYEE_ID
          AddPayeeBankPaymentService.call(self, batch_id, params)
        when PAYEE_TYPE_ACCOUNT
          AddAccountBankPaymentService.call(self, batch_id, params)
        else
          raise Error.new('Invalid payee type provided')
        end
      end

      protected

      def headers
        {
          content_type: :json,
          authorization: "Bearer #{credentials.access_token}"
        }
      end
    end
  end
end