module Fire
  module PaymentBatches
    class AddAccountBankPaymentService < BatchService
      include Constants

      # params
      # EUR
      # {
      #   "icanFrom": "2001",
      #   "payeeType": "ACCOUNT_DETAILS",
      #   "destIban": "IE00AIBK93123412341234",
      #   "destAccountHolderName" :"John Smith",
      #   "amount": "500"
      #   "myRef": "Payment to John Smith for Consultancy in Dec.",
      #   "yourRef": "ACME LTD - INV 23434"
      # }
      # GBP
      # {
      #   "icanFrom": "2001",
      #   "payeeType": "ACCOUNT_DETAILS",
      #   "destNsc": "123456",
      #   "destAccountNumber": "12345678",
      #   "destAccountHolderName" :"John Smith",
      #   "amount": "500"
      #   "myRef": "Payment to John Smith for Consultancy in Dec.",
      #   "yourRef": "ACME LTD - INV 23434"
      # }
      #
      def call
        validate!
        post("#{batch_url}/banktransfers", params)
      end

      private

      def validate!
        raise Exception.new('Invalid parameters provided') if params[:ican_from].blank?
        if params.has_key?(:dest_iban)
          raise Exception.new('Invalid parameters provided') if params[:dest_iban].blank?
        else
          raise Exception.new('Invalid parameters provided') if params[:dest_nsc].blank?
          raise Exception.new('Invalid parameters provided') if params[:dest_account_number].blank?
        end
        raise Exception.new('Invalid parameters provided') if params[:dest_account_holder_name].blank?
        if params[:payee_type].blank? || params[:payee_type] != PAYEE_TYPE_ACCOUNT
          raise Exception.new('Invalid payee type provided')
        end
        raise Exception.new('Invalid transaction amount provided') if params[:amount].to_f <= 0
      end
    end
  end
end