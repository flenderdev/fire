module Fire
  module PaymentBatches
    class RemoveBatchPaymentService < BatchService
      include Constants

      attr_reader :transfer_id, :type

      def initialize(client, batch_id, transfer_id, type = TYPE_BANK_TRANSFER)
        super(client, batch_id, {})
        @transfer_id = transfer_id
        @type = type
      end

      def call
        validate!
        delete(url)
      end

      private

      def validate!
        raise Error.new('Invalid transfer ID provided') if transfer_id.nil? || transfer_id.empty?
        raise Error.new('Invalid type provided') unless TRANSFER_TYPES.include?(type)
      end

      def url
        "#{batch_url}/#{type.to_s.camelize.downcase.pluralize}/#{transfer_id}"
      end
    end
  end
end