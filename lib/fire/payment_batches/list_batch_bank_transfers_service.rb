module Fire
  module PaymentBatches
    class ListBatchBankTransfersService < BatchService
      def call
        get("#{batch_url}/banktransfers")
      end
    end
  end
end