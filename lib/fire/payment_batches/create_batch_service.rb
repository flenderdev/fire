module Fire
  module PaymentBatches
    class CreateBatchService < BaseService
      include Constants

      def call
        validate!
        post('batches', payload)
      end

      private

      def validate!
        raise Error.new('Invalid transfer type provided') unless TRANSFER_TYPES.include?(payload[:type])
        raise Error.new('Invalid currency provided') unless CURRENCIES.include?(payload[:currency])
      end

      def payload
        { type: TYPE_BANK_TRANSFER, currency: EUR }.merge(params.compact)
      end
    end
  end
end