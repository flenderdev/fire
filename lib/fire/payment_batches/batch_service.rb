module Fire
  module PaymentBatches
    class BatchService < BaseService
      attr_reader :batch_id

      def initialize(client, batch_id, params = {})
        super(client, params)
        @batch_id = batch_id
      end

      protected

      def batch_url
        "batches/#{batch_id}"
      end
    end
  end
end