module Fire
  module PaymentBatches
    module Constants
      TRANSFER_TYPES = [
        TYPE_BANK_TRANSFER = 'BANK_TRANSFER'.freeze,
        TYPE_INTERNAL_TRANSFER = 'INTERNAL_TRANSFER'.freeze
      ]

      PAYEE_TYPES = [
        PAYEE_TYPE_PAYEE_ID = 'PAYEE_ID'.freeze,
        PAYEE_TYPE_ACCOUNT = 'ACCOUNT_DETAILS'.freeze
      ]
    end
  end
end