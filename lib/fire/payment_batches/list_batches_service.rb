module Fire
  module PaymentBatches
    class ListBatchesService < BaseService
      def call
        get('batches', params)
      end
    end
  end
end