module Fire
  module PaymentBatches
    class SubmitBatchService < BatchService
      def call
        put(batch_url)
      end
    end
  end
end