module Fire
  module PaymentBatches
    class RetrieveBatchService < BatchService
      def call
        get(batch_url)
      end
    end
  end
end