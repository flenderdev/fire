module Fire
  class BaseClient
    include Connection

    attr_reader :credentials

    # @overload initialize
    #   @param [required, Fire::Credentials] credentials
    #     Your Fire credentials. This should be an instance of the following class:
    #     * `Fire::Credentials` - Used for configuring static, non-refreshing credentials.
    #   When the above parameter not passed, a default value will be initialized with default environment variables:
    #     * `ENV['FIRE_API_CLIENT_ID']`
    #     * `ENV['FIRE_API_CLIENT_KEY']`
    #     * `ENV['FIRE_API_REFRESH_TOKEN']`
    #
    def initialize(credentials = nil)
      @credentials = credentials || Credentials.new
    end
  end
end