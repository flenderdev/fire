module Fire
  class Credentials
    attr_reader :client_id, :client_key, :refresh_token

    # @overload initialize
    #   @param [Hash] options
    #   @option options [required, String] :client_id
    #     The app’s Client ID from the API page in firework online.
    #   @option options [required, String] :client_key
    #     The app’s Client Key from the API page in firework online.
    #     Note: If you ever accidentally reveal the Client Key (or accidentally commit it to Github for instance)
    #           it is vital that you log into firework online and delete/recreate the App Tokens as soon as possible.
    #           Anyone who has these three pieces of data can access the API to view your data and set up payments
    #           from your account (depending on the scope of the tokens).
    #   @option options [required, String] :refresh_token
    #       The app’s Refresh Token from the API page in firework online.
    #
    #   When one of the above options not passed, a default values is searched for in the following locations:
    #     * `ENV['FIRE_API_CLIENT_ID']`
    #     * `ENV['FIRE_API_CLIENT_KEY']`
    #     * `ENV['FIRE_API_REFRESH_TOKEN']`
    #
    def initialize(options = {})
      @client_id = options[:client_id] || ENV['FIRE_API_CLIENT_ID']
      @client_key = options[:client_key] || ENV['FIRE_API_CLIENT_KEY']
      @refresh_token = options[:refresh_token] || ENV['FIRE_API_REFRESH_TOKEN']
    end

    def access_token
      validate!
      if @access_token.nil? || @access_token&.expiry&.to_datetime <= Time.zone.now
        @access_token = Authentication::Client.new(self).access_token
      end
      
      @access_token&.accessToken
    end

    # Removing the secret access key from the default inspect string.
    def inspect
      "#<#{self.class.name} refresh_token=#{refresh_token.inspect} client_id=#{client_id}>"
    end

    def valid?
      !client_id.nil? && !client_id.empty? &&
      !client_key.nil? && !client_key.empty? &&
      !refresh_token.nil? && !refresh_token.empty?
    end

    def validate!
      raise Error.new('Invalid Client Id provided') if client_id.nil? || client_id.empty?
      raise Error.new('Invalid Client Key provided') if client_key.nil? || client_key.empty?
      raise Error.new('Invalid Refresh Token provided') if refresh_token.nil? || refresh_token.empty?
    end
  end
end