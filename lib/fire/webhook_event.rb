require 'ostruct'

module Fire
  class WebhookEvent
    attr_reader :token, :header, :data, :key, :secret

    DEFAULT_ALGORITHM = 'HS256'

    TYPE_LODGEMENT = 'LODGEMENT'

    BATCH_EVENT_OPENED = 'batch.opened'
    BATCH_EVENT_ITEM_ADDED = 'batch.item-added'
    BATCH_EVENT_ITEM_REMOVED = 'batch.item-removed'
    BATCH_EVENT_CANCELLED = 'batch.cancelled'
    BATCH_EVENT_SUBMITTED = 'batch.submitted'
    BATCH_EVENT_APPROVED = 'batch.approved'
    BATCH_EVENT_REJECTED = 'batch.rejected'
    BATCH_EVENT_FAILED = 'batch.failed'
    BATCH_EVENT_COMPLETED = 'batch.completed'

    # @overload initialize
    #   @param [required, String] token
    #   @param [Hash] options
    #   @option options [required, String] :secret
    #     The app’s Secret from the Webhook page in firework online.
    #   @option options [required, String] :key
    #     The app’s Key from the Webhook page in firework online.
    #   When one of the above options not passed, a default values is searched for in the following locations:
    #     * `ENV['FIRE_WEBHOOK_KEY']`
    #     * `ENV['FIRE_WEBHOOK_SECRET']`
    #
    def initialize(token, options = {})
      @token = token
      @key = options[:key] || ENV['FIRE_WEBHOOK_KEY']
      @secret = options[:secret] || ENV['FIRE_WEBHOOK_SECRET']
      @data = JSON.parse(decoded_token[0].to_json, object_class: OpenStruct)
      @header = JSON.parse(decoded_token[1].to_json, object_class: OpenStruct)
    end

    def validate!
      raise Fire::Error.new('Invalid token provided') if token.nil? || token.empty?
      raise Fire::Error.new('Invalid secret provided') if secret.nil? || secret.empty?
      raise Fire::Error.new('Invalid key provided') if key.nil? || key.empty?
      raise Fire::Error.new('Key provided in the request is not recognised') unless key_valid?
    end

    private

    def decoded_token
      JWT.decode(token, secret, true, { algorithm: DEFAULT_ALGORITHM })
    rescue JWT::DecodeError => e
      raise Fire::Error.new(e.message)
    end

    def key_valid?
      header.kid == key && header.alg == DEFAULT_ALGORITHM
    end
  end
end