module Fire
  module Authentication
    class Client < BaseClient
      def access_token
        RetrieveAccessTokenService.call(self)
      end
    end
  end
end