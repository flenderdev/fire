require 'securerandom'
require 'digest'

module Fire
  module Authentication
    class RetrieveAccessTokenService < BaseService
      attr_reader :nonce, :secret

      GRANT_TYPE_ACCESS_TOKEN = 'AccessToken'

      def initialize(client)
        super(client, payload(client))
      end

      def call
        post('apps/accesstokens', params)
      rescue StandardError => error
        puts error.respond_to?(:response) ? error.response.to_s : error.message.to_s
      end

      private

      def secret(client)
        @secret ||= Digest::SHA256.hexdigest("#{nonce}#{client.credentials.client_key}")
      end

      def nonce
        @nonce ||= SecureRandom.random_number(10_000) * Time.now.to_i
      end

      def payload(client)
        {
          client_id: client.credentials.client_id,
          refresh_token: client.credentials.refresh_token,
          nonce: nonce,
          grant_type: GRANT_TYPE_ACCESS_TOKEN,
          client_secret: secret(client)
        }
      end
    end
  end
end